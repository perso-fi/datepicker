import { html, fixture, expect, elementUpdated, nextFrame } from '@open-wc/testing';
import { mock, spy, stub } from "sinon";

import '../pf-datepicker.ts';
import { PfDatepicker } from '../src/date-picker-input/inheritance/PfDatepicker';

const given = beforeEach;
const when = describe;
const then = it;
when('pfDatepicker specs', () => {
  let el: PfDatepicker
  given( async () => {
      el = await fixture(html`
      <pf-datepicker></pf-datepicker>
    `);
  });

  when('PfDatepicker is initialized', () => {
    then('has component pfDatepicker initialized', async () => {
      expect(el).to.exist;
    });
  
    then('it should have a selectedItems property set to []', () => {
      expect(el.selectedItems).to.exist;
      expect(el.selectedItems.length).to.equal(0);
    });
  
    then('if there is not a singleton datepicker, it should automaticcaly create one', () => {
      expect(el['datePicker']).to.exist;
    })
  });
  
  when('user click on datepicker', () => {
    then('it should ask to singleton to attach to', async () => {
      el.click();
      await nextFrame();
      expect(el['datePicker'].show).to.equal(true);
    });

    then('it should have datepicker singleton which is gracefully displayed on side up or bottom or on side of datepicker element', () => {
      
    })
  })
})
