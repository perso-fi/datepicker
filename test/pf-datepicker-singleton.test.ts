import { html, fixture, expect, elementUpdated, nextFrame } from '@open-wc/testing';

import '../pf-datepicker.ts';
import { PfDatePickerSingleton } from '../src/date-picker-singleton/PfDatepickerSingleton';
import { ActiveGrid } from '../src/date-picker-singleton/datepicker-singleton.model';

const given = beforeEach;
const when = describe;
const then = it;
when('pfDatepickerSingleton specs', () => {
  let el: PfDatePickerSingleton
  given( async () => {
      el = await fixture(html`
      <pf-datepicker-singleton></pf-datepicker-singleton>
    `);
  });

  when('Intialization of pfDatepickerSingleton', () => {
      then('it should exist', () => {
          expect(el).to.exist;
      })

      then('it should have days panel displayed as default', () => {
          expect(el.activeGrid).to.equal(ActiveGrid.DAY);
      })
  });

})