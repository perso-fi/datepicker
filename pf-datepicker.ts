import { PfDatepicker } from "./src/date-picker-input/inheritance/PfDatepicker";
import { PfDatepickerSingletonSubscription } from "./src/date-picker-singleton-subscripton/PfDatepickerSingletonSubscription";
import { PfDatePickerSingletonStandard } from "./src/date-picker-singleton-standard/PfDatepickerSingletonStandard";
import { PfDatepickerInputSubscription } from "./src/date-picker-input-subscription/PfDatepickerSubscription";

if (!customElements.get('pf-datepicker')) customElements.define('pf-datepicker', PfDatepicker);
if (!customElements.get('pf-datepicker-singleton')) customElements.define('pf-datepicker-singleton', PfDatePickerSingletonStandard);
if (!customElements.get('pf-datepicker-subscription')) customElements.define('pf-datepicker-subscription', PfDatepickerInputSubscription);
if (!customElements.get('pf-datepicker-singleton-subscription')) customElements.define('pf-datepicker-singleton-subscription', PfDatepickerSingletonSubscription);