import { LitElement, customElement, property, CSSResult, CSSResultArray, css, TemplateResult, html, svg, SVGTemplateResult } from "lit-element";
import { ActiveGrid, TargetDatepickerSingleton } from "./datepicker-singleton.model";
import { SelectDate } from "./inheritance/SelectDate";

// @customElement('pf-datepicker-singleton')
export class PfDatePickerSingleton extends SelectDate {
    private _show: boolean = false;
    private _target: TargetDatepickerSingleton;
    private onOpenDatepickerEventBind: EventListenerObject
    private onCloseSingletonEventBind: EventListenerObject
    private onClickCallbackBodyBind: EventListenerObject;
    private multi: boolean;

    @property({
        type: Boolean, 
        reflect: true
    })
    get show(): boolean {
        return this._show;
    }

    set show(arg) {
        const old = this._show;
        this._show = arg;
        this.showChanged(arg, old);
        this.requestUpdate('show', old);
    }

    @property({
        type: Object
    })
    get target(): TargetDatepickerSingleton {
        return this._target;
    }

    set target(arg: TargetDatepickerSingleton) {
        const old = this._target;
        this._target = arg;
        this.boundTarget = this.setBoundTarget(arg);
        this.targetChanged(arg, old);
        this.requestUpdate('target', old);
    }

    static get basicStyle(): CSSResult {
        return css`
        :host{
            --background-color: white;
            --background-header: #3a913f;
            --background-content: white;
            --color-content: black;
            --color-primary: #4a4a4a;
            --color-hover: var(--color-primary);
            display: flex;
            flex-direction: column;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right:0;
            overflow: hidden;
            color: var(--background-color);
            font-family: sans-serif;
           
        }
        
        :host(:not([show])){
            pointer-events: none;
        }
        
        #window{
            user-select: none;
            border-radius: 4px;
        }
        
        #header{
            background: var(--background-header);
            min-width: 305px;
            min-height: 62px;
            font-family: sans-serif;
            font-size: 15.2px;
            font-weight: 500;
        }

        div[hidden]{
            display: none !important;
        }
        `
    }

    static get styles(): CSSResultArray {
        return [
            this.basicStyle,
            css`
       
        .less{
            transform: rotate(180deg);
        }
        
        #date{
            width: 50%;
        }
        
        #month{
            margin-right: 10px;
        }

        .centerItem {
            display: flex;
            justify-content: center;
        }

        .layout,.horizontal {
            display: flex;
        }

        .vertical {
            flex-direction: column;
        }

        .around-justified {
            justify-content: space-around;
        }

        .center-center {
            align-items: center;
            justify-content: center;
        }

        .justified {
            justify-content: space-between;
        }

        svg {
            height: 15px;
            padding: 2px;
            fill: white;
        }

        svg g {
            pointer-events: none;
        }

        .hoverable:hover {
            fill: var(--color-hover);
            cursor: pointer;
        }
        `, this.styleGrid, this.styleWindow, this.stylePosition ]
    }

    static get styleGrid(): CSSResult {
        return css`
        #content{
            background: var(--background-content);
            margin-top: 37px;
            margin-bottom: 41px;
            color: var(--color-content);
            font-family: sans-serif;
            font-size: 12px;
            font-weight: 500;
            overflow-x: hidden;
        }
        
        
        
        :host([activegrid=month]) #content{
            margin-left: 35px;
            margin-right: 35px;
        }
        
        :host([activegrid=year]) #content{
            margin-left: 35px;
            margin-right: 35px;
        }
        
        .grid{
            display: grid;
            flex: 1;
            grid-auto-rows: 30px;
        }
        
        :host([activegrid=month]) .grid {
            grid-template-columns: repeat(3, 1fr);
            grid-auto-rows: 50px;
            grid-gap: 3px;
        }
        
        .hoverable:hover {
            color: var(--color-primary, #ffd34d);
        }
        
        .grid div{
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-items: center;
            font-size: 13px;
        }
        
        .grid div.activeMonth{
            opacity: 1 !important;
        }
        `
    }

    static get styleWindow(): CSSResult {
        return css`
        
        :host{
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            overflow: hidden;
            
        }
        
        :host #window{
            transform-origin: top center;
            position: absolute;
            background: var(--white);
            overflow: hidden;
            box-sizing: border-box;
            z-index: 99;
            background: white;
            display: flex;
            flex-direction: column;
        }
        
        :host .appear{
            opacity: 0;
        }
        
        :host([show]) .appear{
            opacity: 1;
        }
        
        :host([show]) #window{
            pointer-events: all;
            box-shadow: var(--grey-75) 0px 5px 9px;
        }
        `
    }

    static get stylePosition(): CSSResult {
        return css`
        :host{
            --box-shadow-left: var(--grey-200) 5px -5px 7px, var(--grey-75) 5px -5px 7px;
                
            
            --box-shadow-top: var(--grey-200) -2px -2px 3px, var(--grey-75) -1px -1px 1px;
                
            
            --box-shadow-bottom: var(--grey-200) 5px 5px 9px, var(--grey-75) 5px 5px 9px;
            
            --box-shadow-date: {
                box-shadow: var(--grey-400) 0px 0px 15px, var(--grey-75) 0px 5px 9px;
            };
        }
        
        :host([show]) #window.specialShadow{
            box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.15);
        }

        #cursor{
            width: 10px;
            height: 10px;
            position: absolute;
            background: var(--background-content);
            position: absolute;
            z-index: 99;
            
        }
        `
    }

    render(): TemplateResult{
        return html` 
        <div id="window" class="appear specialShadow">
        <div id="header" class="layout horizontal center-center" @click="${this.handleActionOnHeader}">
        ${this.headerTemplate}
        </div>
        ${this.beforeContent}
        <div id="content" class="layout vertical">
            ${this.gridContentTemplate}
        </div>
    </div>
    <div id="cursor" class="appear"></div>
        `
    }

    get headerTemplate(): TemplateResult {
        return html`
            <svg viewBox="0 0 12 11" id="lessYear" class="less hoverable">${this.iconLessYear}</svg>
            <svg viewBox="0 0 12 11" id="lessMonth" class="less hoverable">${this.iconLessMonth}</svg>
            <div id="date" class="layout horizontal center-center" on-click="_onclickDate">
                <div id="month" class="hoverable">${this.setActiveMonth(this.activeMonth)}</div>
                <div id="year" class="hoverable">${this.activeYear}</div>
            </div>
            <svg viewBox="0 0 12 11" id="moreMonth" class=" hoverable">${this.iconLessMonth}</svg>
            <svg viewBox="0 0 12 11" id="moreYear" class=" hoverable">${this.iconLessYear}</svg>
        </div>
        `
    }

    get beforeContent(): TemplateResult {
        return html`
        `
    }

    get gridMonthTemplate(): TemplateResult {
        return html`
        <div id="gridMonth" class="grid" @click="${this.selectActiveMonthOrYear.bind(this)}">
            ${Array.from({length: 12}, (element, index) => html`<div class="centerItem">${this.translateText.fr.month[index]}</div>`)}
        </div>
        `
    }

    get gridYearTemplate(): TemplateResult {
        return html`
        <div id="gridYear" class="grid" @click="${this.selectActiveMonthOrYear.bind(this)}">
            ${Array.from({length: 20}, (element, index) => html`<div class="centerItem"></div>`)}
        </div>
        `
    }

    get gridContentTemplate(): TemplateResult {
        return html`
        <div class="layout vertical">
            ${this.gridMonthTemplate}
            ${this.gridYearTemplate}
        </div>
        `
    }

    get iconLessYear(): SVGTemplateResult {
        return svg`
            <g id="double_chevron_datepicker" viewBox="0 0 12 11">
                <path id="double_chevron_date_picjer.svg" class="cls-1" d="M708.806,551.989l-0.74-.784,5.183-5.489,0.74,0.784Zm-0.74-10.194,0.74-.784,5.183,5.489-0.74.784Zm-5.337,10.194-0.74-.784,5.183-5.489,0.741,0.784Zm-0.74-10.194,0.74-.784,5.184,5.489-0.741.784Z" transform="translate(-702 -541)"/>
            </g>
        `
    }

    get iconLessMonth(): SVGTemplateResult {
        return svg`
        <g id="chevron_datepicker" viewBox="0 0 5.906 11">
            <path  d="M671.729,551.989l-0.74-.784,5.183-5.489,0.741,0.784Zm-0.74-10.194,0.74-.784,5.184,5.489-0.741.784Z" transform="translate(-671 -541)"/>
        </g>`
    }

    protected handleActionOnHeader(event) {
        
    }
    
    protected get nameOfEventToOpenSingleton(): string {
        return 'open-datepicker';
    }

    connectedCallback() {
        super.connectedCallback();
        this.multi = true;
        this.onOpenDatepickerEventBind = this.onOpenDatepickerEvent.bind(this);
        document.body.addEventListener(this.nameOfEventToOpenSingleton, this.onOpenDatepickerEventBind);
        this.onCloseSingletonEventBind = this.onCloseSingletonEvent.bind(this);
        this.onClickCallbackBodyBind = this.onClickCallbackBody.bind(this);
        this.addEventListener('click', this.onClickCallbackBodyBind);
        document.body.addEventListener('close-singleton', this.onCloseSingletonEventBind);
    }

    private onOpenDatepickerEvent(event) {
        this.target = event.detail.target;
        this.show = event.detail.show != undefined ? event.detail.show : true;
        this.selectedItems = event.detail.selectedItems;
        this.target.singleton = this;
    }

    private onCloseSingletonEvent(event) {
        this.show = false;
    }

    private onClickCallbackBody(event) {
        this.show = event.path.find(item=>item === this.window)? this.multi : false;
    }

    private showChanged(arg, prev) {
        if(this.target)this.target.isConnectedToSingleton = arg;
        if(!arg && prev )this.affectSelectedItemsOnTarget(); // if show is false and it previous value was true and it's not in live mode, we affect selectedItems property on it target
        if(!arg && prev)this.resetElement();// if we had a target element connected and it is not anymore we reset singleton properties
    }

    private targetChanged(arg, prev) {
        if(arg)this.connect(arg);
        if(prev)this.deconnect(prev);
    }

    protected connect(element) {
        element.addEventListener('keyup', element._onTapOnEnterkeyboardBind);
    }

    protected deconnect(element){
        element.attachedList = false;
        element.singleton = undefined;
        element.removeEventListener('keyup', element._onTapOnEnterkeyboardBind);
    }

    private resetElement() {
        this.target = undefined;
        // this.selectAll = false;
    }

    protected affectSelectedItemsOnTarget() {
        // super.affectSelectedItemsOnTarget();
        if(this.target && this.selectedItems != null) this.target.dispatch('update', Array.prototype.concat(this.selectedItems));//target implement ScrollTargetBehavior which implements itself reduxBeahvior
    }

    private deselectItem(item) {
        // for(let i = 0; i <= this.ironList.items.length; i++) {
        //     if(this.ironList.items[i].code === item.code) {
        //         this.ironList.deselectIndex(i);
        //         break;
        //     }
        // }
    }
}