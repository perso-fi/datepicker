import { property, PropertyValues } from "lit-element";
import { Position } from "./Position";
import { ActiveGrid } from "../datepicker-singleton.model";


export class ConstructOfDate extends Position {
    private  _activeMonth: number;
    private  _activeYear: number;
    private  _activeGrid: ActiveGrid = ActiveGrid.DAY;

    connectedCallback() {
        super.connectedCallback();
        this.activeGrid = ActiveGrid.DAY;
    }

    @property({
        type: Number
    })
    get activeMonth() {
        return this._activeMonth;
    }

    set activeMonth(arg) {
        const oldValue = this._activeMonth;
        this._activeMonth = arg;
        this.activeMonthChanged(arg, oldValue);
        this.requestUpdate('activeMonth', oldValue);
    }

    @property({
        type: Number
    })
    get activeYear() {
        return this._activeYear;
    }

    set activeYear(arg) {
        const oldValue = this._activeYear;
        this._activeYear = arg;
        this.activeYearChanged(arg);
        this.requestUpdate('activeYear', oldValue);
    }

    @property({
        type: String,
        reflect: true
    }) 
    get activeGrid(): ActiveGrid {
        return this._activeGrid;
    }

    set activeGrid(arg: ActiveGrid) {
        const old = this._activeGrid;
        this._activeGrid = arg;
        this.updateGridElement();
        this.requestUpdate('activeGrid', old);
        this.setAttribute('activegrid', arg);
    }

    private updateGridElement() {
        for (const key in ActiveGrid) {
            const element: HTMLElement = this[`grid${ActiveGrid[key][0].toUpperCase()}${ActiveGrid[key].substring(1)}`];
            if (!!element) ActiveGrid[key] === this.activeGrid ? element.removeAttribute('hidden') : element.setAttribute('hidden', "");
        }
    }

    firstUpdated(changedProperties: PropertyValues) {
        this.updateGridElement();
    }

    protected get gridMonth(): HTMLElement {
        return this.shadowRoot.querySelector('#gridMonth')
    }

    protected get gridYear(): HTMLElement {
        return this.shadowRoot.querySelector('#gridYear')
    }

    protected activeMonthChanged(arg, prev) {
        if(arg === 12) {
            this.activeYear += 1;
            this.activeMonth = 0;
            return;
        };
        if(arg < 0) {
            this.activeYear -= 1;
            this.activeMonth = 11;
            return;
        };
        Array.prototype.forEach.call(this.gridMonth.children, item => item.classList.remove('selected'));
        this.gridMonth.children[arg].classList.add('selected');
    }

    protected get yearsBeforeActualYear(): number {
        return 9
    }

    protected activeYearChanged(arg: number): void {
        // this.iterateYears(arg);
        Array.prototype.forEach.call(this.gridYear.children, item => item.classList.remove('selected'));
        this.gridYear.children[this.yearsBeforeActualYear-1].classList.add('selected');
    }

    protected iterateYears(arg: number): void {
        for(let i = 0; i < this.gridYear.children.length; i++) {
            this.gridYear.children[i].innerHTML = `${arg - (this.yearsBeforeActualYear-1 - i)}`;
        };
    }

    protected updateSelectedDateElements(item) {}
}