import { Translate } from "./Translate";
import { property } from "lit-element";

export class Position extends Translate {
    private _boundTarget: ClientRect|DOMRect;
    private _shouldBeOnTop: boolean;
    private _canBeBelow: boolean;

    @property({
        type: Object
    })
    get boundTarget(): ClientRect|DOMRect {
        return this._boundTarget
    }

    set boundTarget(arg: ClientRect|DOMRect) {
        const old = this._boundTarget;
        this._boundTarget = arg;
        this.shouldBeOnTop = this.setShouldBeOnTop(arg);
        this.canBeBelow = this.canBeBelowTheTarget(arg);
        this.positionLeft(this.shouldBeOnTop, this.canBeBelow, this.boundTarget);
        this.requestUpdate('boundTarget', old);
    }

    @property({
        type: Boolean
    })
    get shouldBeOnTop(): boolean {
        return this._shouldBeOnTop;
    }

    set shouldBeOnTop(arg) {
        const old = this._shouldBeOnTop;
        this._shouldBeOnTop = arg;
        this.positionLeft(this.shouldBeOnTop, this.canBeBelow, this.boundTarget);
        this.requestUpdate('shouldBeOnTop', old);
    }

    @property({
        type: Boolean
    }) 
    get canBeBelow(): boolean {
        return this._canBeBelow;
    }

    set canBeBelow(arg) {
        const old = this._canBeBelow;
        this._canBeBelow = arg;
        this.positionLeft(this.shouldBeOnTop, this.canBeBelow, this.boundTarget);
        this.requestUpdate('canBeBelow', old);
    }

    protected get window(): HTMLElement {
        return this.shadowRoot.querySelector('#window');
    }

    protected get cursor(): HTMLElement {
        return this.shadowRoot.querySelector('#cursor');
    }

    private get efficientHeight() {
        return this.window.getBoundingClientRect().height + (this.cursor.getBoundingClientRect().height/2);
    }

    protected setBoundTarget(target) {
        if(target) {
            let boundTarget = target.getBoundingClientRect();
            return boundTarget;
        }
    }

    protected setShouldBeOnTop(boundTarget) {
        if(boundTarget)return this.efficientHeight <= boundTarget.top;
    }

    protected canBeBelowTheTarget(boundTarget){
        if(boundTarget)return this.efficientHeight <= window.innerHeight - boundTarget.bottom;
    }

    protected positionLeft(shouldBeOnTop, canBeBelow, boundTarget) {
        if(boundTarget)canBeBelow ? this.placeElement(1) : shouldBeOnTop ? this.placeElement(2) : this.placeElement(3);
    }

    private placeElement(position) {
        //position => 1 = position on below of the target; 2 = position on top of the target ; otherwise position on left of target
        this.cursor.style.transform = '';
        switch(position) {
            case 1:
                this.cursor.style.background = `var(--background-header)`;
                this.cursor.style.boxShadow = "var(--box-shadow-top)";
                this.cursor.style.transform = `translate3d(${this.boundTarget.left + (this.boundTarget.width/2)}px, ${this.boundTarget.top + this.boundTarget.height}px, 0) `;
                this.cursor.style.transform += `rotate(45deg)`;
                this.window.style.top     = `${this.cursor.getBoundingClientRect().top + (this.cursor.getBoundingClientRect().height/2)}px`;
                this.window.style.left    = `${this.cursor.getBoundingClientRect().left - (this.window.getBoundingClientRect().width/2)}px`;
                break;

            case 2:
                this.cursor.style.background = `white`;
                this.cursor.style.boxShadow = "var(--box-shadow-bottom)";
                this.cursor.style.transform = `translate3d(${this.boundTarget.left + (this.boundTarget.width/2)}px, ${this.boundTarget.top - (this.cursor.getBoundingClientRect().height)}px, 0) `;
                this.cursor.style.transform += `rotate(45deg)`;
                this.window.style.top     = `${this.cursor.getBoundingClientRect().top - this.window.getBoundingClientRect().height + (this.cursor.getBoundingClientRect().height/2)}px`;
                this.window.style.left    = `${this.cursor.getBoundingClientRect().left - (this.window.getBoundingClientRect().width/2)}px`;
                break;

            case 3:
                this.cursor.style.background = `white`;
                this.cursor.style.boxShadow = "var(--box-shadow-left)";
                this.cursor.style.transform = `translate3d(${this.boundTarget.left - this.cursor.getBoundingClientRect().width}px, ${this.boundTarget.top + this.boundTarget.height/2 - this.cursor.getBoundingClientRect().height/2}px, 0) `;
                this.cursor.style.transform += `rotate(45deg)`;
                this.window.style.top     = `${this.cursor.getBoundingClientRect().top - this.window.getBoundingClientRect().height/2 + (this.cursor.getBoundingClientRect().height/2)}px`;
                this.window.style.left    = `${this.cursor.getBoundingClientRect().left - this.window.getBoundingClientRect().width + (this.cursor.getBoundingClientRect().width/2)}px`;
                break;
        }
    }
}