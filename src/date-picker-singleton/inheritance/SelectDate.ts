import { ActiveGrid } from "../datepicker-singleton.model";
import { property } from "lit-element";
import { ConstructOfDate } from "./ConstructOfDate";


export class SelectDate extends ConstructOfDate {
    
    private _selectedItems: Date;

    @property({
        type: Date
    })
    get selectedItems(): Date {
        return this._selectedItems;
    }

    set selectedItems(arg) {
        const old = this._selectedItems;
        this._selectedItems = arg;
        this.onSelectedDateChanged(arg);
        this.requestUpdate('selectedItems', old);
    }

    @property({
        type: Number
    }) selectedMonth;

    @property({
        type: Number
    }) selectedYear;

    // protected daysOfWeekVisible(selected) {
    //     return selected === ActiveGrid.DAY ? false : true;
    // }

    // protected activeMonthChanged(arg, prev) {
    //     super.activeMonthChanged(arg, prev);
    //     this.changeDaysDisplayed(this.selectedDay);
    // }

    // protected activeYearChanged(arg) {
    //     super.activeYearChanged(arg);
    //     this.changeDaysDisplayed(this.selectedDay)
    // }

    // protected navYearVisible(selected) {
    //     return selected === ActiveGrid.YEAR ? false : true;
    // }

    protected selectActiveMonthOrYear(event) {
        let index = Array.prototype.reduce.call(event.currentTarget.children, (previous, item, index) =>  item === event.target ? index : previous, -1);
        event.currentTarget === this.gridMonth ? this.activeMonth = index : this.activeYear = parseInt(this.gridYear.children[index].innerHTML);
        this.activeGrid = ActiveGrid.DAY;
    }

    // protected affectSelectedItemsOnTarget(): void {
    //     this.selectedItems = new Date(this.selectedYear, this.selectedMonth);
    // }

    protected onSelectedDateChanged(arg) {
        if(arg) {
            this.activeYear   =   this.selectedYear     = arg.getFullYear();
            this.activeMonth  =   this.selectedMonth    = arg.getMonth();
        }
    }
}