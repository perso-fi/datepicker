import { LitElement } from "lit-element";

export class Translate extends LitElement {
    protected translateText = {
        fr: {
            week: ['DIM', 'LUN', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'],
            month: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
        }
    }

    protected setActiveMonth(activeMonth) {
        return this.translateText.fr.month[activeMonth];
    }
}