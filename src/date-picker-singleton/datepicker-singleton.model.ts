import { PfDatePickerSingleton } from "./PfDatepickerSingleton";

export enum ActiveGrid {
    DAY = "day",
    MONTH = "month",
    YEAR = "year"
}

export interface TargetDatepickerSingleton extends HTMLElement {
    singleton: PfDatePickerSingleton,
    isConnectedToSingleton: boolean,
    dispatch: (arg: string, array: any[]) => void 
}