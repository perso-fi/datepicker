import { PfDatepicker } from "../date-picker-input/inheritance/PfDatepicker";
import { DatePickerSingleton } from "../date-picker-input/inheritance/datepicker.model";
import { CSSResult, css } from "lit-element";


export class PfDatepickerInputSubscription extends PfDatepicker {
    protected onSelectedItemsChanged(arg) {
        if(arg && arg[0])this.inputElement.value = `${(0+(arg[0].getMonth()+1).toString()).slice(-2)} / ${arg[0].getFullYear()}`;
    }

    protected get tagNameSingleton(): string {
        return 'pf-datepicker-singleton-subscription';
    }

    protected get nameOfEventToOpenSingleton(): string {
        return 'open-datepicker-subscription';
    }

    static get styles(): CSSResult { 
        return css`
            ${super.styles}
            :host([has-selected-item]) #input {
                color: var(--color-selected);
            }

            :host([has-selected-item]) #chevron {
                fill: var(--color-selected);
            }

            :host([has-selected-item]) {
                border: var(--color-selected) solid thin;
            }
        `
    }
}