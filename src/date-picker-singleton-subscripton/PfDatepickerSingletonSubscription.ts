import { PfDatePickerSingleton } from "../date-picker-singleton/PfDatepickerSingleton";
import { ActiveGrid } from "../date-picker-singleton/datepicker-singleton.model";
import { SVGTemplateResult ,svg , CSSResultArray,  TemplateResult, html, css } from "lit-element";


export class PfDatepickerSingletonSubscription extends PfDatePickerSingleton {

    static get styles(): CSSResultArray|CSSResultArray {
        return [super.styles, css`
            #content {
                max-height: 276px;
                overflow-y: scroll;
                margin-top: 0px;
                margin-right: 16px;
                margin-left: 16px;
                font-family: Helvetica Neue;
                font-size: 15px;
                line-height : 20px;
            }
            
            #header {
                text-transform: initial;
                font-weight: bold;
            }

            #close {
                position: absolute;
                right: 16px;
                cursor: pointer;
            }

            .selected {
                color: white;
                background: #3A913F;
                border-radius: 4px;
            }

            .centerItem {
                cursor: pointer;
                font-size: 22px;
            }

            .centerItem:hover:not(.selected) {
                background: #E8F5E9;
                border-radius: 4px;
            }

            :host([activegrid=year]) .grid {
                grid-template-columns: repeat(3, 1fr);
                grid-gap: 5px;
            }

            :host([activegrid="year"]) #content {
                margin-left: 16px;
                margin-right: 16px;
            }

            :host([activegrid=month]) .grid {
                grid-template-columns: repeat(4, 1fr);
                grid-auto-rows: 50px;
                grid-gap: 3px;
            }

            :host([activegrid=month]) .grid div {
                font-size: 15px;
            }

            :host([activegrid=year]) .grid div {
                font-size: 22px;
            }

            #beforeContent {
                display: flex;
                flex-direction: column;
            }

            #beforeContent #info {
                font-family: HelveticaNeue, sans-serif;
                font-size: 12px;
                font-weight: normal;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.33;
                letter-spacing: normal;
                color: #b2b2b2;
                padding: 16px;            
            }

            #beforeContent #monthBeforeContent {
                font-family: HelveticaNeue, sans-serif;
                font-size: 18px;
                font-weight: bold;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.33;
                letter-spacing: normal;
                color: #4a4a4a;
                padding: 16px;
            }

            :host([activegrid=month]) #header {
                justify-content: flex-start;
            }

            :host([activegrid=month]) .centerItem {
                margin: 8px 4px;
            }

            :host([activegrid=year]) #header {
                font-family: Helvetica Neue;
                font-weight: bold;
                font-size: 15px;
            }

            #lessMonth {
                margin: 0px 16px;
            }
            :host([show]) #cursor.appear{
                opacity: 0;
            }
        `]
        
    }
    connectedCallback() {
        super.connectedCallback();
        this.activeGrid = ActiveGrid.YEAR;
    }

    protected activeYearChanged(arg) {
        this.iterateYears(new Date(Date.now()).getFullYear());
        Array.prototype.forEach.call(this.gridYear.children, item => parseInt(item.innerText) === arg ? item.classList.add('selected') : item.classList.remove('selected'));
        // this.gridYear.children[this.yearsBeforeActualYear-1].classList.add('selected');
    }

    protected iterateYears(arg: number): void {
        for(let i = this.gridYear.children.length-1; i >= 0; i--) {
            this.gridYear.children[(this.gridYear.children.length-1)-i].innerHTML = `${arg - (this.yearsBeforeActualYear-1 - i)}`;
        };
    }

    get gridYearTemplate(): TemplateResult {
        return html`
        <div id="gridYear" class="grid" @click="${this.selectActiveMonthOrYear.bind(this)}">
            ${Array.from({length:this.yearsBeforeActualYear}, (element, index) => html`<div class="centerItem"></div>`)}
        </div>
        `
    }

    get headerTemplate(): TemplateResult {
        return html`
            ${this.activeGrid !== ActiveGrid.YEAR ? html` <svg viewBox="0 0 12 11" id="lessMonth" class="less hoverable" @click=${this.onClickBack}>${this.iconLessMonth}</svg>`: ""}
            <div>${this.activeGrid === ActiveGrid.YEAR ? "Choisissez une année": this.activeYear}</div>
            <svg id="close" width="16" height="16" viewBox="0 0 16 16" @click="${this.onClickClose}">
            ${this.closeButton}
            </svg>
        </div>
        `
    }

    get closeButton(): SVGTemplateResult {
        return svg`<path fill="#FFF" fill-rule="evenodd" d="M3.53 2.47L8 6.939l4.47-4.47c.293-.292.767-.292 1.06 0 .293.294.293.768 0 1.061L9.061 8l4.47 4.47c.292.293.292.767 0 1.06-.294.293-.768.293-1.061 0L8 9.061l-4.47 4.47c-.293.292-.767.292-1.06 0-.293-.294-.293-.768 0-1.061L6.939 8l-4.47-4.47c-.292-.293-.292-.767 0-1.06.294-.293.768-.293 1.061 0z"/>`
    }

    get beforeContent(): TemplateResult {
        return html`
        <div id="beforeContent">
            <span id="info">Si vous ne connaissez pas la date exacte,  estimez la.</span>
            ${this.activeGrid === ActiveGrid.MONTH ? html`<span id="monthBeforeContent">Choisissez un mois</span>`: ""}
        </div>
        `
    }

    protected get yearsBeforeActualYear(): number {
        return 99;
    }

    protected get nameOfEventToOpenSingleton(): string {
        return 'open-datepicker-subscription';
    }

    onClickClose(event) {
        this.onClickBack(event);
        this.show = false;
    }

    onClickBack(event) {
        event.stopImmediatePropagation();
        this.activeGrid = ActiveGrid.YEAR;
    }

    protected selectActiveMonthOrYear(event) {
        let index = Array.prototype.reduce.call(event.currentTarget.children, (previous, item, index) =>  item === event.target ? index : previous, -1);
        if (event.currentTarget === this.gridMonth)  {
            event.stopImmediatePropagation();
            this.activeMonth = this.selectedMonth = index;
            this.show = false;
        }
        if (event.currentTarget === this.gridYear) {
            this.activeYear = this.selectedYear = parseInt(this.gridYear.children[index].innerHTML);
            this.activeGrid = ActiveGrid.MONTH;
        }
    }

    protected affectSelectedItemsOnTarget(): void {
        this.selectedItems = new Date(this.selectedYear, this.selectedMonth, 1);
        super.affectSelectedItemsOnTarget();
    }

}