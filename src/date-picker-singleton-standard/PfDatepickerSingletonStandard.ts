import { PfDatePickerSingleton } from "../date-picker-singleton/PfDatepickerSingleton";
import { ActiveGrid } from "../date-picker-singleton/datepicker-singleton.model";
import { CSSResult, css, TemplateResult, html, property, CSSResultArray } from "lit-element";
import { ConstructOfDateMixin } from "./Mixin/ConstructOfDate";


export class PfDatePickerSingletonStandard extends (PfDatePickerSingleton) {
    private  _selectedDay: number;
    @property({
        type: Number
    })
    get selectedDay(): number {
        return this._selectedDay;
    }

    set selectedDay(arg) {
        const oldValue = this._selectedDay;
        this._selectedDay = arg;
        this.changeDaysDisplayed(this.selectedDay)
        this.requestUpdate('selectedDay', oldValue);
    }

    protected onSelectedDateChanged(arg) {
        if(arg) {
            this.selectedDay    =   arg.getDate();
            this.activeYear   =   this.selectedYear     = arg.getFullYear();
            this.activeMonth  =   this.selectedMonth    = arg.getMonth();
        }
    }

    protected affectSelectedItemsOnTarget(): void {
        this.selectedItems = new Date(this.selectedYear, this.selectedMonth, this.selectedDay);
        super.affectSelectedItemsOnTarget();
    }

    protected activeMonthChanged(arg, prev) {
        super.activeMonthChanged(arg, prev);
        this.changeDaysDisplayed(this.selectedDay);
    }

    protected activeYearChanged(arg) {
        this.iterateYears(arg);
        super.activeYearChanged(arg);
        this.changeDaysDisplayed(this.selectedDay)
    }

    protected selectActiveDay(event) {
        this.selectedMonth = this.activeMonth;
        this.selectedYear = this.activeYear;
        this.selectedDay = undefined;
        this.selectedDay = parseInt(event.target.innerHTML);
    }

    protected get gridDay(): HTMLElement {
        return this.shadowRoot.querySelector('#gridDay');
    }

    private placeLastDayOfPreviousMonth() {
        let lastDayOfPrevMonth = new Date(this.activeYear, this.activeMonth, 0).getDate();
        let indexOfLastDateOfPrevMonth = new Date(this.activeYear, this.activeMonth, 0).getDay();
        if(this.gridDay.children[indexOfLastDateOfPrevMonth])
        {
            this.gridDay.children[indexOfLastDateOfPrevMonth].innerHTML = `${lastDayOfPrevMonth}`;
            for(let i = indexOfLastDateOfPrevMonth; i>= 0; i--) {
                this.gridDay.children[i].innerHTML = `${lastDayOfPrevMonth - (indexOfLastDateOfPrevMonth-i)}`;
                this.addClassDay(i, false);
            }
        }
        return indexOfLastDateOfPrevMonth;
    }

    private placeActualMonth(index) {
        let lastDayOfCurrentMonth = new Date(this.activeYear, this.activeMonth+1, 0).getDate();
        for(let i = 1; i < lastDayOfCurrentMonth+1; i++) {
            this.gridDay.children[index+i].innerHTML = `${i}`;
            this.addClassDay(index+i, true);
        }
        if(index+lastDayOfCurrentMonth < this.gridDay.children.length-1)this.placeNextMonth(index+lastDayOfCurrentMonth);
    }

    private placeNextMonth(index) {
        for(let i =  1; i <= this.gridDay.children.length -1 - index; i++) {
            this.gridDay.children[index + i].innerHTML = `${i}`;
            this.addClassDay(index+i, false);
        }
    }

    private addClassDay(index, isInCurrentMonth) {
        isInCurrentMonth ? this.gridDay.children[index].classList.add('activeMonth') : this.gridDay.children[index].classList.remove('activeMonth');
    }

    protected changeDaysDisplayed(selectedDay) {
        if(this.activeMonth != undefined && this.activeYear && selectedDay) {
            let index = this.placeLastDayOfPreviousMonth();
            this.placeActualMonth(index);
            this.displayActiveDay();
        }
    }

    private displayActiveDay() {
        Array.prototype.forEach.call(this.gridDay.children, item => {
            item.classList.remove('selectedDay');
            this.updateSelectedDateElements(item)
        })
    }

    protected updateSelectedDateElements(item): void {
        if(this.selectedYear === this.activeYear && this.activeMonth === this.selectedMonth && parseInt(item.innerHTML) === this.selectedDay && item.classList.contains('activeMonth'))item.classList.add('selectedDay')
    }

    static get styleGrid(): CSSResult {
        return css`
        ${super.styleGrid}
            #dayOfWeek{
                color: var(--color-content);
                margin-bottom: 15px;
            }
            
            #navYear{
                margin-top: 15px;
                text-transform: uppercase;
            }

            :host([activegrid=day]) #content{
                margin-left: 35px;
                margin-right: 35px;
            }
        
            :host([activegrid=day]) .grid div{
                opacity: .3;
            }
            :host(:not([activegrid=day])) .grid div{
                background: var(--background-content);
                border-radius: 3px;
            }
            
            :host(:not([activegrid=day])) .grid div:hover{
                background: var(--color-primary);
            }

            :host([activegrid=year]) .grid {
                grid-template-columns: repeat(4, 1fr);
                grid-gap: 5px;
            }
            
            :host([activegrid=day]) .grid .activeMonth:hover{
                color: black;
            }
        
            :host([activegrid=day]) .grid .selectedDay{
                background: var(--background-content);
                color: var(--color-primary);
                border-radius: 50%;
                font-size: 12px;
                font-weight: 600;
            }

            :host([activegrid=day]) .grid {
                grid-template-columns: repeat(7, 1fr);
                grid-gap: 5px;
            }

            :host(:not([activegrid=day])) .grid div.selected{
                transition: opacity  .15s ease-in-out;
                background: var(--background-content);
                font-size: 12px;
                font-weight: 600;
            }
        `
    }

    static get stylePosition(): CSSResult {
        return css`
        ${super.stylePosition}
        
        `
    }

    static get styles(): CSSResultArray {
        return super.styles;
    }

    get gridDayTemplate(): TemplateResult {
        return html`
        <div id="gridDay" class="grid " @click="${this.selectActiveDay.bind(this)}">
            ${Array.from({length: 42}, (element, index) => html`<div class="centerItem"></div>`)}
        </div>
        `
    }

    get navYear(): TemplateResult {
        return html`
        <div id="navYear" class="horizontal layout justified" ?hidden="${this.navYearVisible(this.activeGrid)}" @click="${this.navYearClicked}">
            <span id="lessAlot">précédent</span>
            <span id="moreAlot">suivant</span>
        </div>
        `
    }

    get gridContentTemplate(): TemplateResult {
        return html`
        <div class="layout vertical">
            ${this.gridDayTemplate}
            ${this.gridMonthTemplate}
            ${this.gridYearTemplate}
        </div>
        ${this.navYear}
        `
    }

    get beforeContent(): TemplateResult {
        return html`
        <div id="dayOfWeek" class="layout horizontal around-justified" ?hidden="${this.daysOfWeekVisible(this.activeGrid)}">
            <div>SUN</div>
            <div>MON</div>
            <div>TUE</div>
            <div>WED</div>
            <div>THU</div>
            <div>FRI</div>
            <div>SAT</div>
        </div>
        `
    }

    private daysOfWeekVisible(selected) {
        return selected === ActiveGrid.DAY ? false : true;
    }

    protected navYearClicked(event) {
        event.target === this.shadowRoot.querySelector('#moreAlot') ? this.activeYear += 20 : this.activeYear -= 20;
    }

    protected handleActionOnHeader(event) {
        switch(event.target) {
            case this.shadowRoot.querySelector('#moreMonth'):
                this.activeMonth += 1;
                break;
                
                case this.shadowRoot.querySelector('#lessMonth'):
                this.activeMonth -= 1;
                break;
                    
                case this.shadowRoot.querySelector('#moreYear'):
                this.activeYear += 1;
                break;

            case this.shadowRoot.querySelector('#lessYear'):
                this.activeYear -= 1;
                break;

            case this.shadowRoot.querySelector('#month'):
                this.activeGrid = ActiveGrid.MONTH;
                break;

            case this.shadowRoot.querySelector('#year'):
                this.activeGrid = ActiveGrid.YEAR;
                break;
        }
    }

    private navYearVisible(selected) {
        return selected === ActiveGrid.YEAR ? false : true;
    }
}