import { PfDatePickerSingleton } from "../../date-picker-singleton/PfDatepickerSingleton";



export function ConstructOfDateMixin<T extends new(...args: any[]) => T>(Base: new(...args: any[])=> PfDatePickerSingleton): PfDatePickerSingleton {
    return new class extends Base {
        protected get gridDay(): HTMLElement {
            return this.shadowRoot.querySelector('#gridDay');
        }
    
        private placeLastDayOfPreviousMonth() {
            let lastDayOfPrevMonth = new Date(this.activeYear, this.activeMonth, 0).getDate();
            let indexOfLastDateOfPrevMonth = new Date(this.activeYear, this.activeMonth, 0).getDay();
            if(this.gridDay.children[indexOfLastDateOfPrevMonth])
            {
                this.gridDay.children[indexOfLastDateOfPrevMonth].innerHTML = `${lastDayOfPrevMonth}`;
                for(let i = indexOfLastDateOfPrevMonth; i>= 0; i--) {
                    this.gridDay.children[i].innerHTML = `${lastDayOfPrevMonth - (indexOfLastDateOfPrevMonth-i)}`;
                    this.addClassDay(i, false);
                }
            }
            return indexOfLastDateOfPrevMonth;
        }
    
        private placeActualMonth(index) {
            let lastDayOfCurrentMonth = new Date(this.activeYear, this.activeMonth+1, 0).getDate();
            for(let i = 1; i < lastDayOfCurrentMonth+1; i++) {
                this.gridDay.children[index+i].innerHTML = `${i}`;
                this.addClassDay(index+i, true);
            }
            if(index+lastDayOfCurrentMonth < this.gridDay.children.length-1)this.placeNextMonth(index+lastDayOfCurrentMonth);
        }
    
        private placeNextMonth(index) {
            for(let i =  1; i <= this.gridDay.children.length -1 - index; i++) {
                this.gridDay.children[index + i].innerHTML = `${i}`;
                this.addClassDay(index+i, false);
            }
        }
    
        private addClassDay(index, isInCurrentMonth) {
            isInCurrentMonth ? this.gridDay.children[index].classList.add('activeMonth') : this.gridDay.children[index].classList.remove('activeMonth');
        }
    
        protected changeDaysDisplayed(selectedDay) {
            if(this.activeMonth != undefined && this.activeYear && selectedDay) {
                let index = this.placeLastDayOfPreviousMonth();
                this.placeActualMonth(index);
                this.displayActiveDay();
            }
        }
    
        private displayActiveDay() {
            Array.prototype.forEach.call(this.gridDay.children, item => {
                item.classList.remove('selectedDay');
                this.updateSelectedDateElements(item)
            })
        }
    }
}