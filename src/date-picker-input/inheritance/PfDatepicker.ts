import { customElement, html, css, TemplateResult, SVGTemplateResult, svg,  property, CSSResult, CSSResultArray }  from 'lit-element';
import { SelectedBehavior } from './SelectedBehavior';
import { DatePickerSingleton } from './datepicker.model';

// @customElement('pf-datepicker')
export class PfDatepicker extends SelectedBehavior {
    private _autofocus: boolean = false;
    private _value: string = "";
    // private _baseList: any[] = [];
    // private _finalList: any[] = [];
    private _isConnectedToSingleton: boolean = false;
    private _singleton: DatePickerSingleton;
    
    // @property({
    //     reflect: true,
    //     type: Boolean
    // })  show = false;

    // @property({
    //     type: Boolean,
    //     reflect: true,
    // })
    // get autofocus() {
    //     return this._autofocus;
    // }

    // set autofocus(arg) {
    //     const oldValue = this._autofocus;
    //     if(arg === true) {
    //         this._autofocus = arg;
    //         this.inputElement.focus();
    //     }
    //     this.requestUpdate('autofocus', oldValue);
    // }

    @property({
        type: String,

    })
    get value() {
        return this._value;
    }

    set value(arg) {
        const oldValue = this._value;
        this._value = arg;
        this.dispatchEvent(new CustomEvent('value-changed', {detail:{value: arg}, bubbles: true, composed: true}));
        // this.filterBaseList(this.baseList, this.value);
        this.requestUpdate('value', oldValue);
    }

    @property({
        type: String,
        reflect: true
    }) placeholder = "Choisir une date";

    @property({
        type: Boolean,
        reflect: true
    })
    get isConnectedToSingleton(): boolean {
        return this._isConnectedToSingleton;
    }

    set isConnectedToSingleton(value: boolean) {
        const oldValue = this._isConnectedToSingleton;
        this._isConnectedToSingleton = value;
        if(value !== oldValue) {
            // this._isConnectedToSingletonChanged(value);
            if(value)this.fireEventToSingleton();
            this.requestUpdate('isConnectedToSingleton', oldValue);
        }
    }

    @property({
        type: Object
    })
    get singleton(): DatePickerSingleton {
        return this._singleton;
    }

    set singleton(value) {
        const oldValue = this._singleton;
        this._singleton = value;
        this.onSingletonSet(value, oldValue);
        // this.updateSingletonDataWithFinalList(this.singleton, this.finalList);
        this.requestUpdate('singleton', oldValue);
    }

    // @property({
    //     type: Array
    // })
    // get baseList(): any[] {
    //     return this._baseList
    // }

    // set baseList(arg) { 
    //     const oldValue = this._baseList;
    //     this._baseList = arg;
    //     // this.baseListChanged(arg);
    //     this.filterBaseList(this.baseList, this.value);
    //     this.requestUpdate('baseList', oldValue);
    // }

    // @property({
    //     type: Array
    // })
    // get finalList(): any[] {
    //     return this._finalList;
    // }

    // set finalList(arg) {
    //     const oldValue = this._finalList;
    //     this._finalList = arg;
    //     this.updateSingletonDataWithFinalList(this.singleton, this.finalList);
    //     this.requestUpdate('finalList', oldValue);
    // }

    // @property({
    //     type: Boolean
    // }) live = false;

    @property({
        type: Object
    }) event = new CustomEvent('open-singleton', {detail: {target: this, selectedItems: this.selectedItems}, bubbles: true, composed: true});

    static get styles(): CSSResult {
        return css`
        :host{
            --color-primary: #4a4a4a;
            --color-selected: #3A913F;
            --color-input: var(--color-primary);
            --color-icon-calendar: var(--color-primary);
            --background-color: #ffffff;
            --width-input: 288px;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 12px 16px;
            color: var(--color-input);
            background: var(--background-color);
            outline: none;
            width: var(--width-input);
            box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.15);
            border-radius: 5px;
            font-family: sans-serif;
            cursor: pointer;
        }
        
        :host([hidden]) {
            display: none;
        }
        
        input{
            display: flex;
            flex: 1;
            border: 0px solid var(--grey-75);
            outline: none;
            margin-left: 16px;
            font-size: 15px;
            line-height: 1.33;
            text-align: left;
            color: #4a4a4a;
            pointer-events: none;
        }
        
        #calendar {
            color: var(--color-icon-calendar);
        }
        `
    }

    private get iconDate(): SVGTemplateResult {
        return svg`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" id="calendar">
            <path fill="#3A913F" fill-rule="evenodd" d="M15.556 1.5c.276 0 .5.224.5.5l-.001 1.299 2.167.001c1.258 0 2.278 1.02 2.278 2.278v12.644c0 1.258-1.02 2.278-2.278 2.278H5.778C4.52 20.5 3.5 19.48 3.5 18.222V5.578C3.5 4.32 4.52 3.3 5.778 3.3l2.166-.001V2c0-.276.224-.5.5-.5.277 0 .5.224.5.5v1.299h6.111V2c0-.276.224-.5.5-.5zM19.5 9.7h-15v8.522c0 .706.572 1.278 1.278 1.278h12.444c.706 0 1.278-.572 1.278-1.278V9.7zM7.944 4.299L5.778 4.3c-.706 0-1.278.572-1.278 1.278v3.121h15V5.578c0-.706-.572-1.278-1.278-1.278l-2.167-.001V5.6c0 .276-.223.5-.5.5-.276 0-.5-.224-.5-.5V4.299H8.944V5.6c0 .276-.223.5-.5.5-.276 0-.5-.224-.5-.5V4.299z"/>
        </svg>        `
    }

    private get chevronRight(): SVGTemplateResult {
        return svg`<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" id="chevron">
                        <path fill-rule="evenodd" d="M9.69 7.75L4.22 2.28c-.293-.293-.293-.767 0-1.06.293-.293.767-.293 1.06 0l6 6c.293.293.293.767 0 1.06l-6 6c-.293.293-.767.293-1.06 0-.293-.293-.293-.767 0-1.06l5.47-5.47z"/>
                    </svg>`
    }

    protected get inputTemplate(): TemplateResult {
        return html`
        ${this.iconDate}
        <input
        type="text"
        placeholder="${this.placeholder}"
        id="input"
        @focus="${this.onFocus}"
        />
        ${this.chevronRight}
        `
    }

    protected get datePicker(): DatePickerSingleton|null {
        return document.body.querySelector(this.tagNameSingleton);
    }

    protected get inputElement(): any {
        return this.shadowRoot.querySelector('#input');
    }

    protected get tagNameSingleton(): string {
        return 'pf-datepicker-singleton';
    }

    protected get nameOfEventToOpenSingleton(): string {
        return 'open-datepicker';
    }

    render() {
    return html`
    ${this.inputTemplate}
        `;
    }

    connectedCallback() {
        super.connectedCallback();
        this.addEventListener('click', this.onClickEvent.bind(this));
        // this.event = new CustomEvent('open-singleton', {detail: {target: this, selectedItems: this.selectedItems, live: this.live, filtered: this.isFiltered()}, bubbles: true, composed: true});
        if (this.datePicker === null) document.body.appendChild(document.createElement(this.tagNameSingleton));
    }

    private onClickEvent(event: MouseEvent) {
        event.stopImmediatePropagation();//avoid side effect on dom click event
        this.isConnectedToSingleton = true;
    }

    public reset() {
        this.selectedItems = [];
        this.inputElement.value = "";
    }

    protected fireEventToSingleton(day: number|false = false, month: number|false = false, year: number|false = false) {
        let date = new Date(Date.now());
        let selectedDate = this.selectedItems[0] ? this.selectedItems[0] : new Date(year ? year : date.getFullYear(), month ? month-1 : date.getMonth(), day ? day : date.getDate());
        this.dispatchEvent(new CustomEvent(this.nameOfEventToOpenSingleton, {detail: {target: this, selectedItems: selectedDate}, bubbles: true, composed: true}));
    }

    private onFocus(event) {
        setTimeout(()=>{
            if(this.inputElement && this.inputElement.value)this.inputElement.setSelectionRange(0,this.inputElement.value.length);
        },50);

    }
}
