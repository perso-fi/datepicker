

export interface DatePickerSingleton extends Element {
    selectItem: (arg: any) => void;
    show: boolean
}

export interface itemList {
    label: string
}