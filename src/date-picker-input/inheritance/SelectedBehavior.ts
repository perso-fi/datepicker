import { property } from "lit-element";
import { itemList } from "./datepicker.model";
import { SingletonBehavior } from "./SingletonBehavior";

export class SelectedBehavior extends SingletonBehavior {
    // selectedItems: any[];
    finalList: any[];
    protected input: HTMLInputElement;
    private _selectedItems: any[] = [];

    @property({
        type: Array,
    })
    get selectedItems(): any[] {
        return this._selectedItems;
    }

    set selectedItems(value: any[]) {
        const oldValue = this._selectedItems;
        this._selectedItems = value;
        this.dispatchEvent(new CustomEvent('selected-items-changed', { detail: { value }, bubbles: true,composed: true}));
        this.onSelectedItemsChanged(value);
        this.requestUpdate('selectedItems', oldValue);
        value ? this.setAttribute('has-selected-item', "") : this.removeAttribute('has-selected-item');
    }

    protected onSelectedItemsChanged(arg) {
        if(arg && arg[0])this.inputElement.value = `${(0+arg[0].getDate().toString()).slice(-2)}/${(0+(arg[0].getMonth()+1).toString()).slice(-2)}/${arg[0].getFullYear()}`;
    }
    
    // public removeItem(item: any) {
    //     const index = this.checkItemInSelectedList(item)
    //     if( !!index ) {
    //         let a  = this.selectedItems.concat();
    //         a.splice(index, 1);
    //         this.dispatch('update', a)
    //     }
    // }
    
    // private checkItemInSelectedList(item: any): number|false {
    //     return this.selectedItems.indexOf(item) != -1 ? this.selectedItems.indexOf(item) : false;
    // }

    private reduxEvent: CustomEvent = new CustomEvent('selected-items-redux-changed', {detail: {}, bubbles: true, composed: true});

    protected dispatch(action, info) {
        switch(action) {
            case "update":
            this.affectDetailAndFireEvent(info);
            break;
        }
    }

    private affectDetailAndFireEvent(info) {
        this.reduxEvent.detail.value = info;
        this.selectedItems = info;
        this.dispatchEvent(this.reduxEvent);
    }

    // protected filterBaseList(baseList: any[], value: string) {
    //     if(baseList)this.finalList = value ? baseList.filter(item=>this.filterAlgoOnCriterionObject(value, item)): baseList;
    // }

    // private filterAlgoOnCriterionObject(inputValue: string, itemList: itemList) {
    //     return itemList.label.toLocaleLowerCase().indexOf(inputValue.toLowerCase()) != -1;
    // }

    protected onSingletonSet(arg, prev) {
        if(arg)this.addEventListener('selected-items-changed', this.onSelectedSingletonBind);
        if(prev)this.removeEventListener('selected-items-changed', this.onSelectedSingletonBind);
    }
    
}