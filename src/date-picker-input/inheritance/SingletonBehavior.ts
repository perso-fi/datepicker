import { LitElement } from "lit-element";
import { DatePickerSingleton } from "./datepicker.model";

export class SingletonBehavior extends LitElement {
    // selectedItems: any[];
    live: boolean;
    value: string;
    event: CustomEvent;
    // attachedList: boolean;
    protected onSelectedSingletonBind: EventListenerObject;
    protected onTapOnEnterkeyboardBind: EventListenerObject;
    finalList: any[];
    protected input: HTMLInputElement;
    singleton: DatePickerSingleton

    connectedCallback() {
        super.connectedCallback();
        this.onSelectedSingletonBind = this.onSelectedInSingleton.bind(this);
        // this.onTapOnEnterkeyboardBind = this.onTapOnEnterkeyboard.bind(this);
        // this.event = new CustomEvent('open-singleton', {detail: {target: this, selectedItems: this.selectedItems, live: this.live, filtered: this.isFiltered()}, bubbles: true, composed: true});
    }

    protected isFiltered() {
        return this.value !== "" && this.value !== undefined;
    }

    // protected _isConnectedToSingletonChanged(arg) {
    //     this.dispatchEvent(new CustomEvent('attached-list-changed', {detail: {value: arg}, bubbles: true, composed: true}));// fire event composed to pierce the shadow dom
    //     if(arg)this.fireEventToSingleton();// element connect to singleton irisScrolllist
    //     if(arg === false)this.value = "";//we reset the filter on baseList and finalList
    // }

    // protected fireEventToSingleton() {
    //     let a;
    //     if(this.finalList) a = this.finalList.concat();
    //     this.event.detail.data = a;
    //     this.dispatchEvent(this.event);
    // }

    // protected updateSingletonDataWithFinalList(singleton, finalList){
    //     if(finalList && singleton) {
    //         if(this.isFiltered()) {
    //             singleton.data = finalList;
    //             singleton.selectedItems = this.selectedItems;
    //         }
    //     }
    // }

    private onSelectedInSingleton(event) {
        //if(!this.live)this.singleton.show = false;// element isn't in live, we close singleton
    }

    // protected onTapOnEnterkeyboard(event) {
    //     if(event.code === "Enter" || event.keyCode === 13) {//if user press enter key
    //         if(this.finalList.length === 1)this.singleton.selectItem(this.finalList);
    //         this.singleton.show = false;
    //         this.input.blur();
    //     }
    // }
}