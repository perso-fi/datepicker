ARG REPOSITORY_NAME=openwc-app-template
FROM ${REPOSITORY_NAME}:latest as base
ARG TAG_NAME
COPY ./${TAG_NAME}.ts /usr/src/components/${TAG_NAME}/${TAG_NAME}.ts
COPY ./index.ts /usr/src/components/${TAG_NAME}/index.ts
COPY ./src /usr/src/components/${TAG_NAME}/src
COPY ./stories /usr/src/components/${TAG_NAME}/stories
COPY ./test /usr/src/components/${TAG_NAME}/test
COPY ./demo /usr/src/components/${TAG_NAME}/demo
COPY ./index.html /usr/src/index.html
COPY ./package.json /usr/src/package.json
RUN ls -la /usr/src/components/${TAG_NAME}
RUN ln -s /usr/src/dist /usr/src/dist2vol
WORKDIR /usr/src
RUN yarn

FROM base as publish
RUN yarn build
