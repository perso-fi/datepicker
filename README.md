

## Datepicker web-component 

[![Built with open-wc recommendations](https://img.shields.io/badge/built%20with-open--wc-blue.svg)](https://github.com/open-wc)

## Presentation
---
the component generates a singleton which is used by as many component instances you want.
this project offers two differents types of date picker: 
  - `standard`: <br>
  ```html 
  <pf-datepicker></pf-datepicker>
  ```
  - `custom` which only proposes to choose year and month.
  ```html 
  <pf-datepicker-subscription></pf-datepicker-subscription>
  ```
## prerequisite
---
If you want integrate in IE11, you have to dynamically integrate polyfils.
in your index.html, you have to add
```html
<script src="https://unpkg.com/@webcomponents/webcomponentsjs@2.4.3/webcomponents-bundle.js"></script>
``` 
## Installation 
---
### _`in a script tag`_
just paste in your index.html: <br>
```html
<script src="https://unpkg.com/@pedrule/datepicker@0.0.1/dist/pf-datepicker.js"></script>
```
### _`as module`_
declare in your project as a dependency.
You just have to import as a module in your es6 module: 
```js
import '@pedrule/datepicker';
```
## integration in your project
---
just declare dedicated tagname in html files where you want integrate datepicker.
- `standard`: <br>
  ```html 
  <pf-datepicker></pf-datepicker>
  ```
  - `custom` which only proposes to choose year and month.
  ```html 
  <pf-datepicker-subscription></pf-datepicker-subscription>
  ```

## Api
---
### `Input`
|name|required|type|default|
|---|---|---|---|
|value|no|string|""|
|placeholder|no|string|'Choisir une date'|

### `Output`
|name|type|path|return|
|---|---|---|---|
|selected-items-changed|CustomEvent|event.detail.value|[Date]|

exemple: 
```html
<pf-datepicker-subscription id="datepicker"></pf-datepicker-subscription>
```

```js
const datepicker = document.body.querySelector('#datepicker');
datepicker.addEventListener('selected-items-changed', event => console.log(event.detail.value[0]));
```
### `Input CSS`
|name|default|
|---|---|
|--color-primary|#4a4a4a|
|--color-selected|#3A913F|
|--color-input|var(--color-primary)|
|--color-icon-calendar|var(--color-primary)|
|--background-color|#ffffff|
|--width-input|288px|

## Inheritance to build your specialization
---
you can inherit from base class of each component to add your own features.
```js
import { PfDatepicker } from '@pedrule/datepicker';

export class MyOwnDatepicker extends PfDatepicker {
    
}
``` 